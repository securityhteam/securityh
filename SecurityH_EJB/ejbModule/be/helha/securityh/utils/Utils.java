package be.helha.securityh.utils;

import org.mindrot.jbcrypt.BCrypt;

public class Utils {
	public static String createHash(String string) {
		return BCrypt.hashpw(string, BCrypt.gensalt());
	}
	
	public static boolean checkHash(String string1, String string2) {
		return BCrypt.checkpw(string1, string2);
	}
}
